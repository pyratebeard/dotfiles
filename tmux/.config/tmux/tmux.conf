# ░▀█▀░█▄█░█░█░█░█░░
# ░░█░░█░█░█░█░▄▀▄░░
# ░░▀░░▀░▀░▀▀▀░▀░▀░░

# change prefix to backtick
unbind C-b
set-option -g prefix `
bind ` send-prefix

# unbind detach
# can still use D to select
# a client to detach
unbind d

# shell
set -g default-command /usr/bin/zsh
set -g default-shell /usr/bin/zsh

# start with window 1 (instead of 0)
set -g base-index 1

# start with pane 1
set -g pane-base-index 1

# keep window names fixed
set-option -g allow-rename off

# rename window on creation
## these break tmuxp, but i like them
## instead run ad-hoc command on last opened pane in tmuxp config
## see dotfiles/tmux/.config/tmux/main.yaml
#set-hook -g after-new-window 'command-prompt -I "" "rename-window '%%'"'
#set-hook -g after-new-session 'command-prompt -I "" "rename-window '%%'"'

# screen mode
set -g default-terminal "screen-256color"

# source config file
bind r source-file $XDG_CONFIG_HOME/tmux/tmux.conf

# history
set -g history-limit 4096

# allow terminal scrolling
set-option -g terminal-overrides 'xterm*:smcup@:rmcup@'
set-option -sa terminal-features ",rxvt-unicode-256color:RGB"

# vim style copy paste mode
#unbind [
#bind Escape copy-mode
#bind-key -T copy-mode-vi 'v' send -X begin-selection
#bind-key -T copy-mode-vi 'y' send -X copy-pipe-and-cancel \
#  "/usr/bin/xsel -i -p > /dev/null"\; \
#  display-message "copied selection to primary clipboard"
unbind [
bind Escape copy-mode
unbind i
bind i paste-buffer
bind-key -Tcopy-mode-vi 'v' send -X begin-selection
#bind-key -Tcopy-mode-vi 'y' send -X copy-pipe-and-cancel 'xclip -in -selection primary'
bind-key -Tcopy-mode-vi 'y' send -X copy-pipe-and-cancel 'xsel -i >/dev/null'

# use vi mode
setw -g mode-keys vi
set -g status-keys vi

# splitting
unbind %
bind b split-window -h
unbind '"'
bind v split-window -v

# window switching
# urxvt tab like window switching (-n: no prior escape seq)
bind-key -n S-left prev
bind-key -n S-right next
bind-key -n C-left swap-window -t -1
bind-key -n C-right swap-window -t +1

# pane switching
unbind h
bind h select-pane -L
unbind j
bind j select-pane -D
unbind k
bind k select-pane -U
unbind l
bind l select-pane -R

# colon :
bind : command-prompt

# pomodoro
bind-key g run-shell "~/.tmux/plugins/tmux-golden-apple/golden_apple.tmux start"
bind-key G run-shell "~/.tmux/plugins/tmux-golden-apple/golden_apple.tmux stop"

# panes
set -g pane-border-style fg=black
set -g pane-active-border-style fg=colour244

# messaging
set -g message-style fg=colour7,bg=colour8
set -g message-command-style fg=colour0,bg=colour1
set-option -g display-time 4000

# window mode
setw -g mode-style fg=colour7,bg=colour5

# resizing
setw -g aggressive-resize on

# status bar
set -g status-justify right
set -g status-style bg=terminal
set -g status-fg colour7
set -g status-interval 5
set -g status-right-length 100
setw -g window-status-separator " "

setw -g window-status-format "#[bg=colour241,fg=colour0] #I #[bg=colour241,fg=colour0]#W #[bg=default,fg=colour241]▓░"
setw -g window-status-current-format "#[bg=colour10,fg=colour0] #I #[bg=colour10,fg=colour0]#W #[bg=default,fg=colour10]▓░"
set -g status-justify left
set-option -g status-right '#[bg=default,fg=colour237]░▓#[bg=colour237,fg=colour15]#{goldenapple}#{muted_state}#{bond_device}#{online_status}#[bg=colour237,fg=colour243] %Y%m#[bg=colour237,fg=colour7]%d#[fg=colour243]-#[fg=colour15]%u #[fg=colour7]%H%M #[bg=colour243,fg=colour237]▓#[default]'
#set-option -g status-right '#[bg=default,fg=colour237]░▓#[bg=colour237,fg=colour15]#(~/bin/tmux-status) #{goldenapple}#{muted_state}#{bond_device}#{online_status}#[bg=colour237,fg=colour243] %Y%m#[bg=colour237,fg=colour7]%d#[fg=colour243]-#[fg=colour15]%u #[fg=colour7]%H%M #[bg=colour243,fg=colour237]▓#[default]'
set-option -g status-left ''

# online and offline icon for tmux-online-status
set -g @online_icon "#[bg=colour237,fg=colour241]░▓█#[bg=colour241,fg=black]online#[bg=colour237,fg=colour241]█▓░#[default]"
set -g @offline_icon "#[bg=colour237,fg=colour1]░▓█#[bg=colour1,fg=white]offline#[bg=colour237,fg=colour1]█▓░#[default]"

# device names for tmux-bond-device
set -g @ethernet "#[bg=colour237,fg=colour7] hardwire #[default]"
set -g @wifi "#[bg=colour237,fg=colour7] airborne #[default]"

# device names for tmux-muted
set -g @muted "#[bg=colour237,fg=colour241]░▓█#[bg=colour241,fg=black]mute#[bg=colour237,fg=colour241]█▓░#[default]"
set -g @unmuted "#[bg=colour237,fg=colour1]░▓█#[bg=colour1,fg=white]on air#[bg=colour237,fg=colour1]█▓░#[default]"

# work minutes for pomodoro
set -g @work 15

# loud or quiet?
set-option -g visual-activity on
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

# tmux clock
set -g clock-mode-colour colour6

# escape time (for vim)
set -sg escape-time 0

# some key-binding changes
#bind x kill-pane
bind X next-layout
bind Z previous-layout

# toggle status bar visibility
#bind t set status

# toggle todo list popup
## http://git.z3bra.org/scripts/file/todo.html
unbind t
bind t display-popup -w 75 ~/.local/bin/todo

unbind T
bind T command-prompt -I "run-shell 'todo "

# plugins
# `mkdir -p ~/.tmux/plugins/ ; git clone 'https://github.com/tmux-plugins/tpm' ~/.tmux/plugins/tpm`
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-online-status'
set -g @plugin 'https://gitlab.com/pyratebeard/tmux-bond-device.git'
set -g @plugin 'tmux-plugins/tmux-muted'

# see: toggle on/off all keybindings · Issue #237 · tmux/tmux - https://github.com/tmux/tmux/issues/237
# Also, change some visual styles when window keys are off
bind-key -T root F12  \
	set-option -g prefix None \;\
	setw -g window-status-current-format "#[bg=colour1,fg=colour0] #I #[bg=colour1,fg=colour0]#W #[bg=colour0,fg=colour1]▓░" \;\
	refresh-client -S

bind-key -T root F11 \
	set-option -g prefix ` \;\
	bind ` send-prefix \;\
	setw -g window-status-current-format "#[bg=colour10,fg=colour0] #I #[bg=colour10,fg=colour0]#W #[bg=colour0,fg=colour10]▓░" \;\
	refresh-client -S

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.config/tmux/plugins/tpm/tpm'
