return {
	"oldriceputin",
	name = "oldriceputin",
	lazy = false,
	dev = { true },
	priority = 1000,
	config = function()
		vim.cmd("colorscheme oldriceputin")
	end,
}
