
#https://github.com/shabble/irssi-docs/wiki/complete_themes
# default foreground color (%N) - -1 is the "default terminal color"
default_color = "-1";

# print timestamp/servertag at the end of line, not at beginning
info_eol = "false";

# these characters are automatically replaced with specified color
# (dark grey by default)
replaces = { "[]" = "%W$*%n"; };

abstracts = {

  ############# generic ###############################

  indent_default = " + %X7m│%n";

  # text to insert at the beginning of each non-message line
  # line_start = "%g//%n ";
  line_start = "";

  # timestamp styling, nothing by default
  timestamp = "";

  # any kind of text that needs hilighting, default is to bold
  hilight = "%c$*%n";

  # any kind of error message, default is bright red
  error = "%R$*%n";

  # channel name is printed
  channel = "%b$*%n";

  # nick is printed
  nick = "%_$*%_";

  # nick host is printed
  nickhost = "%g$*";

  # server name is printed
  server = "%_$*%_";

  # some kind of comment is printed
  comment = "%W(\"%Y$*%W\"%W)%n";

  # reason for something is printed (part, quit, kick, ..)
  reason = "%w(\"%W$*%w\")%n";

  # mode change is printed ([+o nick])
  mode = "%W(\"%y$*%W\"%W)%n";

  ## channel specific messages

  # highlighted nick/host is printed (joins)
  channick_hilight = "%g$*%N";
  chanhost_hilight = "{nickhost $*}";

  # nick/host is printed (parts, quits, etc.)
  channick = "$*";
  chanhost = "{nickhost $*}";

  # highlighted channel name is printed
  channelhilight = "%y$*%n";

  # ban/ban exception/invite list mask is printed
  ban = "$*";

  # trackbar colour
  trackbar_style = "%g";

  ########### messages #################################

  # the basic styling of how to print message, $0 = nick mode, $1 = nick
  # msgnick = "$1-$0%K·%N %|";
  msgnick = "%g$0%R$1%X7m │ %N";
  # msgnick = "%K$0%N $1 %K· %|%N";

  # $0 = nick mode, $1 = nick
  #ownmsgnick = "{msgnick $0 $1}";
  ownmsgnick = "%Y$0%W$1%X7m │ %y";
  ownnick = "%W$*%n";

  # public message in channel, $0 = nick mode, $1 = nick
  pubmsgnick = "{msgnick $0 $1}";
  pubnick = "%m$*%n";

  # public highlighted message in channel
  pubmsghinick = "{msgnick $0 $1}";
  menick = "%C$*%n";

  # channel name is printed with message
  msgchannel = "%K:%m$*%n";

  # private message, $0 = nick, $1 = host
  privmsg = "$0=%b\"$1-\"%n ";

  # private message from you, $0 = "msg", $1 = target nick
  ownprivmsg = "$0=%b\"$1-\"%n ";

  # private message in query
  # privmsgnick = "{msgnick $*}";
  privmsgnick = " %m$*%X7m │ %N";

  # own private message in query
  #ownprivmsgnick = "{privmsgnick $*}";
  ownprivmsgnick = " %W$*%X7m │ %y";
  #ownprivnick = "$*";


  ########## Actions (/ME stuff) #########################

  # generic one that's used by most actions
  action = "%b$* %X7m│ %b";

  # own action, both private/public
  # ownaction = "{action $*}";
  ownaction = "%b$* %X7m│ %b";

  # own action with target, both private/public
  ownaction_target = "{action $*}";

  # private action sent by others
  pvtaction = "{action $*}";
  pvtaction_query = "{action $*}";

  # public action sent by others
  pubaction = "{action $*}";


  ########## other IRC events #############################

  # whois
  whois = "%#    $[8]0 = $1-;";

  # notices
  ownnotice = "%NNote n = %Mnew%n $0 ($1-) ";
  notice = "%M$*%n ";
  pubnotice_channel = " %N($*)";
  pvtnotice_host = " %N($*)";
  servernotice = " %N($*)";

  # CTCPs
  ownctcp = "%NCTCP c = %Mnew%n $0 ($1-) ";
  ctcp = "%N$*%n";

  # wallops
  wallop = "%K$*%n: ";
  wallop_nick = "%n$*";
  wallop_action = "%K * $*%n ";

  # netsplits
  netsplit = "%Wsplit %X7m┄┄─┤ %n\"$*\"%n";
  netjoin = "%Wnjoin %X7m┄┄─┤ %n\"$*\"%n";

  # /names list
  names_prefix = "%X7m│%n $1";
  names_nick = "%_$2$0%_%n$1 ";
  names_nick_op = "{names_nick $* %9%y}";
  names_nick_halfop = "{names_nick $* %G}";
  names_nick_voice = "{names_nick $* %M}";
  names_users = "%X7m┌─┄┄─┄┄──┘%W users in $1";
  names_channel = "%G$*%n";

  # DCC
  dcc = "%g$*%n";
  dccfile = "%_$*%_";

  # DCC chat, own msg/action
  dccownmsg = "%g /* $0 ($1-) */";
  dccownnick = "$*%n";
  dccownquerynick = "$*%n";
  dccownaction = "{action $*}";
  dccownaction_target = "{action $*}";

  # DCC chat, others
  dccmsg = "%g/* $1- ($0) */";
  dccquerynick = "%g$*%n";
  dccaction = "{action $*}";

  ######## statusbar/topicbar ############################

  # default background for all statusbars. You can also give
  # the default foreground color for statusbar items.
  sb_background = "%9";

  # default backround for "default" statusbar group
  #sb_default_bg = "%4";
  # background for prompt / input line
  sb_prompt_bg = "%n";
  # background for info statusbar
  #sb_info_bg = "%8";
  # background for topicbar (same default)
  sb_topic_bg = "%n";

  # text at the beginning of statusbars. sb-item already puts
  # space there,so we don't use anything by default.
  sbstart = "";
  # text at the end of statusbars. Use space so that it's never
  # used for anything.
  sbend = "";

  topicsbstart = " %Wtopic %X7m┄┄─┤$*";
  topicsbend = "$*";

  prompt = "          %X7m└──┄┄%n ";
  # prompt = " %K└╼ ";

  sb = "          %X7m│%n %W$*%c %n";
  sbmode = " %b[%y%y+%C$*%b]";
  sbaway = " %g/* zZzZ */%n";
  sbservertag = ":%g$0%n";
  sbnickmode = "%y$0%W";

  # Usercount
  sb_usercount = "{sb %_$0%_ nicks ($1-)}";
  sb_uc_ircops = "%_*%_$*";
  sb_uc_ops = "%_@%r$*%n";
  sb_uc_halfops = "%_%%%g$*%n";
  sb_uc_voices = "%_+%y$*%n";
  sb_uc_normal = "%w$*%n";
  sb_uc_space = " ";
  # activity in statusbar

  # ',' separator
  sb_act_sep = " ";
  # normal text
  sb_act_text = "%x7K%9%k$*%n";
  # public message
  sb_act_msg = "%7%9%k$*%n";
  # hilight
  sb_act_hilight = "%2%9%k$*%n";
  # hilight with specified color, $0 = color, $1 = text
  sb_act_hilight_color = "%R%9$1-%n";
  # required for adv_windowlist
  #sb_act_none = "%x7M%m$*%n";
  sb_act_none = "%x7m%9%k$*%n";
};

########################################################

formats = {
  "fe-common/core" = {
    #line_start = "{line_start}";
    line_start_irssi = "{line_start}";

    servertag = "         %X7m│%n ";

    join = "%W join %X7m┄┄─┤ %n{channick $0} %W(%w{nickhost %w$1}%W) joined %w$2";
    part = "%W part %X7m┄┄─┤ %n{channick $0} %W(%w{nickhost %w$1}%W) left %w$2 {reason %b$3}";
    quit = "%W quit %X7m┄┄─┤ %Wsignoff: %n{channick $0} %W{reason $2}";
    kick = "%r kick %X7m┄┄─┤ %n{channick $0} was kicked by $2 %W{reason %b$3}";
    quit_once = "{channel $3} {channick $0} {chanhost $1} %kquit {reason %C$2}";

    nick_changed = "        %X7m┄┼┄%W %n{channick $0} %Y>> %w{channick $1}";
    your_nick_changed = "        %X7m┄┼┄%W %n{channick $0} %Y>> %w{channick $1}";
    your_nick = "%W nick %X7m┄┄─┤ %n{channick $0}";

    pubmsg = "{pubmsgnick $2 {pubnick $[-7]0}}$1";

    own_msg = "{ownmsgnick $2 {ownnick $[-7]0}}$1";
    own_msg_channel = "{ownmsgnick $3 {ownnick $[-7]0}{msgchannel $1}}$2";
    own_msg_private_query = "{ownprivmsgnick {ownprivnick $[-7]2}}$1";

    pubmsg_me = "{pubmsghinick $2 {menick $[-7]0}}$1";
    pubmsg_me_channel = "{pubmsghinick $3 {menick $[-7]0}{msgchannel $1}}$2";

    pubmsg_hilight = "{pubmsghinick $3 {menick $[-7]1}}$2";
    pubmsg_hilight_channel = "{pubmsghinick $0 $[-7]1$4{msgchannel $2}}$3";

    pubmsg_channel = "{pubmsgnick {pubnick %g$[-7]0}$2}$1";
    msg_private_query = "{privmsgnick $[-7]0}$2";
    new_topic = "%X7m┌─┄┄─────┘%n\n%X7m│ %wtopic %M'$2' %Nset by%y $0\n%X7m└─┄┄─┄┄──┐ ";

    endofnames = "%X7m└─┄┄─┄┄──┐ %WTotal: {hilight $2} ops, {hilight $3} halfops, {hilight $4} voices, {hilight $5} normal%n";

    daychange = " %W date %X7m┄┄─┤ %Wday changed to %w%%Y%%m%%d";
    config_reloaded = "%W conf %X7m┄┄─┤ %Wreloaded";
    config_modified = "%W conf %X7m┄┄─┤ %Wmodified";
    server_changed = "%W serv %X7m┄┄─┤ %Wchanged to %w$1 %G$2%W";
    looking_up = "%wlooking up %w{server $0}";
    connecting = "%wconnecting to {server $0} [%B$1] port %Y$2";
    connection_established = "%wconnection to {server $0} established";
    disconnected = "%wdisconnected from {server $0} %b{reason $1}";
    server_quit = "%wdisconnected from {server $0} %b{reason $1}";
    connection_lost = "%wconnection lost to {server $0}";
    server_reconnect_removed = "%wremoved reconnection to server {server $0} port {hilight $1}";
    no_away_msgs = "%wno new messages in awaylog";
    current_channel =  "         %X7m│ %Wcurrent channel {channel %Y$0}";
    chanlist_header = "         %X7m│ %Wyou are in the following channels";
    chanlist_line = "         %X7m│ %#{channel %G$[-10]0} %|%W+$1 %w$2";

    not_enough_params = "         %X7m│ %Wnot enough parameters given";
  };
  "fe-common/irc" = {
    chanmode_change = "%W mode %X7m┄┄─┤ %w{channick $1}%W by %w$2";
    server_chanmode_change = "%W mode %X7m┄┄─┤ %w{channick $1}%W (%w$2%W)";
    usermode_change = "%wmode {mode $0}%w for {nick $1}";

    action_core = "%c$[-8] %b│ %c$1";
    own_action = "{ownaction $[-8]0}$1";
    action_private = "{pvtaction $[-8]0}$1";
    action_private_query = "{pvtaction_query $[-8]0} $2";
    action_public = "{pubaction $[-8]0}$1";
    action_public_channel = "{pubaction $[-8]0{msgchannel $1}}$2";
    topic = "%wtopic %X7m┄┄─┤ %G$0 %M$1";
    no_topic = "%Ytopic %c┄┄─┤ %Wno topic for %y$0";
    topic_info = "         %X7m│ %Wset by %y$0 %w(%b$1%w)";
    channel_created = "         %X7m│ %bcreated %W$1";
    channel_synced = "%W sync %X7m┄┄─┤%b in {hilight $1} secs";
    url = "         %X7m│ %Wurl: %g$1";
    notice_private = "         %X7m│{notice $0{pvtnotice_host $1}}$2";
    notice_public = "%X7m│pubnot{notice $0{pubnotice_host $1}}$2";
    notice_server = "%X7m│servnot{servernotice $0}$1";
    own_notice = "%X7m│ownnot{ownnotice notice $0}$1";
  };
  "fe-common/irc/dcc" = {
    dcc_unknown_ctcp = "{dcc DCC unknown ctcp {hilight $0} from {nick $1} [$2]}";
  };
  "Irssi::Script::ichat" = { ichat_timestamp = "%Y>%W>      $1 %y>%n"; };
  "Irssi::Script::adv_windowlist" = {
    awl_mouse = "no";
    awl_display_key = "$H $N $C %8▓░$S";
    awl_display_key_active = "$H%x0a%9%X7b $N $C %9%k%8▓░$S";
    awl_display_header = "%9%x7g%9%X7o $C %0%k▓░";
    awl_separator = " ";
    awl_detach = "(status)";
  };
};
